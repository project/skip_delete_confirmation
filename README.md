CONTENTS OF THIS FILE
---------------------

 * SUMMARY
 * Installation
 * Configuration
 * Maintainers

-- SUMMARY --

This module allows you to skip delete page of confirmation.


REQUIREMENTS
------------

No special requirements.


-- INSTALLATION --

* Install as usual, see:
https://www.drupal.org/documentation/install/modules-themes/modules-7
for further information.


-- CONFIGURATION --

 - Go to (Home » Administration » Structure » types) then
 choose your content type then scroll dowen and checked the
 Skip delete confirmation for this content type.


-- Maintainers --
Current maintainers:
* Abdulaziz Alshamiri (a_alshamiri) - https://www.drupal.org/u/a_alshamiri
* Essam AlQaie (3ssom) - https://www.drupal.org/u/3ssom
